<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**Doctores */
Route::get('doctores', 'Api\Fichero\DoctorController@index');
Route::get('doctores/{id}/', 'Api\Fichero\DoctorController@show')->where('id', '[0-9]+');
Route::post('doctores/', 'Api\Fichero\DoctorController@store');
Route::put('doctores/{id}/', 'Api\Fichero\DoctorController@update')->where('id', '[0-9]+');
Route::get('doctores-inactivo/{id}/', 'Api\Fichero\DoctorController@changeOK')->where('id', '[0-9]+');
Route::get('doctores-especialidades', 'Api\Fichero\DoctorController@getDoctor');


/**Doctor Paciente Hospital */
Route::get('doctor-paciente-hospital', 'Api\Fichero\DoctorPacienteHospital@index');
Route::get('doctor-paciente-hospital/{id}/', 'Api\Fichero\DoctorPacienteHospital@show')->where('id', '[0-9]+');
Route::post('doctor-paciente-hospital/', 'Api\Fichero\DoctorPacienteHospital@store');
Route::put('doctor-paciente-hospital/{id}/', 'Api\Fichero\DoctorPacienteHospital@update')->where('id', '[0-9]+');
Route::get('doctor-paciente-hospital-inactivo/{id}/', 'Api\Fichero\DoctorPacienteHospital@changeOK')->where('id', '[0-9]+');

/**Especialidad */
Route::get('especialidades', 'Api\Fichero\EspecialidadController@index');
Route::get('especialidades/{id}/', 'Api\Fichero\EspecialidadController@show')->where('id', '[0-9]+');
Route::post('especialidades/', 'Api\Fichero\EspecialidadController@store');
Route::put('especialidades/{id}/', 'Api\Fichero\EspecialidadController@update')->where('id', '[0-9]+');
Route::get('especialidades-inactivo/{id}/', 'Api\Fichero\EspecialidadController@changeOK')->where('id', '[0-9]+');

/**Hospitales */
Route::get('hospitales', 'Api\Fichero\HospitalController@index');
Route::get('hospitales/{id}/', 'Api\Fichero\HospitalController@show')->where('id', '[0-9]+');
Route::post('hospitales/', 'Api\Fichero\HospitalController@store');
Route::put('hospitales/{id}/', 'Api\Fichero\HospitalController@update')->where('id', '[0-9]+');
Route::get('hospitales-inactivo/{id}/', 'Api\Fichero\HospitalController@changeOK')->where('id', '[0-9]+');

/**Nota visita */
Route::get('nota-visita', 'Api\Fichero\NotaVisitaController@index');
Route::get('nota-visita/{id}/', 'Api\Fichero\NotaVisitaController@show')->where('id', '[0-9]+');
Route::post('nota-visita/', 'Api\Fichero\NotaVisitaController@store');
Route::put('nota-visita/{id}/', 'Api\Fichero\NotaVisitaController@update')->where('id', '[0-9]+');
Route::get('nota-visita-inactivo/{id}/', 'Api\Fichero\NotaVisitaController@changeOK')->where('id', '[0-9]+');

/**Pacientes */
Route::get('pacientes', 'Api\Fichero\PacienteController@index');
Route::get('pacientes/{id}/', 'Api\Fichero\PacienteController@show')->where('id', '[0-9]+');
Route::post('pacientes/', 'Api\Fichero\PacienteController@store');
Route::put('pacientes/{id}/', 'Api\Fichero\PacienteController@update')->where('id', '[0-9]+');
Route::get('pacientes-inactivo/{id}/', 'Api\Fichero\PacienteController@changeOK')->where('id', '[0-9]+');
