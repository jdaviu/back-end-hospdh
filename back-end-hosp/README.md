Hospital DH
===========

Aplicación desarrollada con Laravel back end, 

Consideraciones
--------------------
+ La base de datos debe ser Mysql o MariaDB
+ Antes de ejecutar algun comando crear la Base de Datos "hospital"


Instrucciones
--------------------
+ 1.- Clonar el repositorio
+ 2.- Descargar las dependencias, ejecutar el comando composer install
+ 3.- Crear el archivo .env en la raiz del proyecto
+ 4.- Copiar el contenido del archivo .env.example al archivo .nev
+ 5.- Ejecutar las migraciones con el comando php artisan migrate
+ 6.- Para poblar la base de datos ejecutar el comando php artisan db:seed
+ 7.- Para levantar el proyecto ejecutar el comando php artisan serve --port 5050 --host 0.0.0.0 