<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class HospitalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('hospitales')->insert([
            'nombre'=>'Hospital del Norte',
            'telefono'=>'4425252',
            'direccion'=>'Av. Temporal nro 852',
            'creado_por'=>'Seeder',
            'actualizado_por'=>'Seeder',

        ]);
    }
}
