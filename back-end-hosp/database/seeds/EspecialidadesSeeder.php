<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EspecialidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('especialidades')->insert([
            'nombre'=>'Pediatría',
            'descripcion'=>'Especilidad con enfoque a pacientes menores, niños',
            'creado_por'=>'Seeder',
            'actualizado_por'=>'Seeder',

        ]);

        DB::table('especialidades')->insert([
            'nombre'=>'Odontología',
            'descripcion'=>'Especilidad curación bucal, atiende a todas las edades',
            'creado_por'=>'Seeder',
            'actualizado_por'=>'Seeder',

        ]);
    }
}
