<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pacientes')->insert([
            'nombre'=>'Julio Rafael',
            'apellidos'=>'Daviú Arévalo',
            'fecha_nacimiento'=>'1982-07-31',
            'direccion'=>'Calle Ruiz de Orellana 1185',
            'creado_por'=>'Seeder',
            'actualizado_por'=>'Seeder',

        ]);


        DB::table('pacientes')->insert([
            'nombre'=>'José Ignacio',
            'apellidos'=>'Daviú Montaño',
            'fecha_nacimiento'=>'2016-06-30',
            'direccion'=>'Calle Ruiz de Orellana 1185',
            'creado_por'=>'Seeder',
            'actualizado_por'=>'Seeder',

        ]);
    }
}
