<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        DB::table('hospitales')->insert([
            'nombre'=>'Hospital del Norte',
            'telefono'=>'4425252',
            'direccion'=>'Av. Temporal nro 852',

        ]);

        DB::table('pacientes')->insert([
            'nombre'=>'Julio Rafael',
            'apellidos'=>'Daviú Arévalo',
            'fecha_nacimiento'=>'1982-07-31',
            'direccion'=>'Calle Ruiz de Orellana 1185',


        ]);


        DB::table('pacientes')->insert([
            'nombre'=>'José Ignacio',
            'apellidos'=>'Daviú Montaño',
            'fecha_nacimiento'=>'2016-06-30',
            'direccion'=>'Calle Ruiz de Orellana 1185',


        ]);

        DB::table('especialidades')->insert([
            'nombre'=>'Pediatría',
            'descripcion'=>'Especilidad con enfoque a pacientes menores, niños',

        ]);

        DB::table('especialidades')->insert([
            'nombre'=>'Odontología',
            'descripcion'=>'Especilidad curación bucal, atiende a todas las edades',

        ]);

        DB::table('doctores')->insert([
            'nombre'=>'Cinthya',
            'apellidos'=>'Olmos Rojas',
            'fecha_nacimiento'=>'1955-05-20',
            'direccion'=>'Av. Ayacucho nro 1055',
            'especialidad_id'=>1,

        ]);

        DB::table('doctores')->insert([
            'nombre'=>'Teresa',
            'apellidos'=>'Rivero Romero',
            'fecha_nacimiento'=>'1989-05-20',
            'direccion'=>'Av. Forestal nro 1023',
            'especialidad_id'=>2,


        ]);
    }
}
