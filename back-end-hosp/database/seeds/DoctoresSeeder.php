<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DoctoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('doctores')->insert([
            'nombre'=>'Cinthya',
            'apellidos'=>'Olmos Rojas',
            'fecha_nacimiento'=>'1955-05-20',
            'direccion'=>'Av. Ayacucho nro 1055',
            'especialidad_id'=>1,
            'creado_por'=>'Seeder',
            'actualizado_por'=>'Seeder',

        ]);

        DB::table('doctores')->insert([
            'nombre'=>'Teresa',
            'apellidos'=>'Rivero Romero',
            'fecha_nacimiento'=>'1989-05-20',
            'direccion'=>'Av. Forestal nro 1023',
            'especialidad_id'=>2,
            'creado_por'=>'Seeder',
            'actualizado_por'=>'Seeder',

        ]);
    }
}
