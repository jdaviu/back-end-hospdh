<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',80);
            $table->string('apellidos',150);
            $table->date('fecha_nacimiento');
            $table->string('direccion',200);
            $table->unsignedBigInteger('especialidad_id');
            $table->timestamps();


             //Llave foranea
             $table->foreign('especialidad_id')
             ->references('id')->on('especialidades')
             ->onDelete('cascade');

        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctores');
    }
}
