<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorPacienteHospitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_paciente_hospital', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('doctor_id');
            $table->unsignedBigInteger('paciente_id');
            $table->unsignedBigInteger('hospital_id');
            $table->string('creado_por',80);
            $table->string('actualizado_por',80);
            $table->timestamps();


             //Llave foranea
             $table->foreign('paciente_id')
             ->references('id')->on('pacientes')
             ->onDelete('cascade');

             $table->foreign('doctor_id')
             ->references('id')->on('doctores')
             ->onDelete('cascade');

             $table->foreign('hospital_id')
             ->references('id')->on('hospitales')
             ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_paciente_hospital');
    }
}
