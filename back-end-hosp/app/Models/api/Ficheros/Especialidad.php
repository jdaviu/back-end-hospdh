<?php

namespace App\Models\Api\Ficheros;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    //
    protected $table='especialidades';

    protected $fillable=[
        'nombre',
        'apellidos',
        'descripcion',       
    ];

    protected $hidden=['created_at','updated_at'];
}
