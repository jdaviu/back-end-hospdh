<?php

namespace App\Models\Api\Ficheros;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    //
    protected $table='hospitales';

    protected $fillable=[
        'nombre',
        'telefono',
        'direccion',       
    ];

    protected $hidden=['created_at','updated_at'];
}
