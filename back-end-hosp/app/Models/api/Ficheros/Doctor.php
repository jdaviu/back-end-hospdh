<?php

namespace App\Models\Api\Ficheros;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    //
    protected $table='doctores';

    protected $fillable=[
        'nombre',
        'apellidos',
        'fecha_nacimiento',
        'direccion',
        'especialidad_id',        
    ];

    protected $hidden=['created_at','updated_at'];
}
