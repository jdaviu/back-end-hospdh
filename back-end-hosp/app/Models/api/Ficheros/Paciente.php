<?php

namespace App\Models\Api\Ficheros;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    //

    protected $table='pacientes';

    protected $fillable=[
        'nombre',
        'apellidos',
        'fecha_nacimiento',
        'direccion',
        
    ];

    protected $hidden=['created_at','updated_at'];
}
