<?php

namespace App\Models\Api\Ficheros;

use Illuminate\Database\Eloquent\Model;

class DoctorPacienteHospital extends Model
{
    //
    protected $table='doctor_paciente_hospital';

    protected $fillable=[
        'doctor_id',
        'paciente_id',
        'hospital_id',       
    ];

    protected $hidden=['created_at','updated_at','creado_por','actualizado_por'];
}
