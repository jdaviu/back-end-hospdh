<?php

namespace App\Models\Api\Ficheros;

use Illuminate\Database\Eloquent\Model;

class NotaVisita extends Model
{
    //
    protected $table='notas_visitas';

    protected $fillable=[
        'paciente_id',
        'descripcion',
        'fecha_visita',       
    ];

    protected $hidden=['created_at','updated_at','creado_por','actualizado_por'];
}
