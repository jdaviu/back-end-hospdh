<?php

namespace App\Http\Controllers\Api\Fichero;

use App\Models\Api\Ficheros\Paciente;
use App\Http\Controllers\Api\Fichero\BaseController;
use App\Http\Requests\Api\Ficheros\Paciente\StoreRequest;
use App\Http\Requests\Api\Ficheros\Paciente\UpdateRequest;
use Illuminate\Http\Request;

class PacienteController extends BaseController
{
    //
    //Listar
    public function index()
    {
        $list = Paciente::all();
        return $this->sendResponse($list, "LISTA RECUPERADA");
    }
    //Recuperar por id
    public function show(Paciente $id)
    {
        return $this->sendResponse($id, "ID RECUPERADO");
    }
    //Editar  por id
    public function update(UpdateRequest $request, Paciente $id)
    {
        $id->update($request->all());
        return $this->sendResponse($id, "ID EDITADO");
    }
    //Eliminar  por id
    public function destroy(Paciente $id)
    {
        $id->delete();
        return $this->sendResponse($id, "ID ELIMINADO");
    }

    //Crear
    public function store(StoreRequest $request)
    {
        $id = Paciente::create($request->all());
        return $this->sendResponse($id, "CREADO CORRECTAMENTE");
    }
}
