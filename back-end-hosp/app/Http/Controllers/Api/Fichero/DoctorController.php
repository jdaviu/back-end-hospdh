<?php

namespace App\Http\Controllers\Api\Fichero;

use App\Models\Api\Ficheros\Doctor;
use App\Http\Controllers\Api\Fichero\BaseController;
use App\Http\Requests\Api\Ficheros\Doctor\StoreRequest;
use App\Http\Requests\Api\Ficheros\Doctor\UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoctorController extends BaseController
{
    //

    //Listar
    public function index()
    {
        $list = Doctor::all();
        return $this->sendResponse($list, "LISTA RECUPERADA");
    }
    //Recuperar por id
    public function show(Doctor $id)
    {
        return $this->sendResponse($id, "ID RECUPERADO");
    }
    //Editar  por id
    public function update(UpdateRequest $request, Doctor $id)
    {
        $id->update($request->all());
        return $this->sendResponse($id, "ID EDITADO");
    }
    //Eliminar  por id
    public function destroy(Doctor $id)
    {
        $id->delete();
        return $this->sendResponse($id, "ID ELIMINADO");
    }

    //Crear
    public function store(StoreRequest $request)
    {
        $id = Doctor::create($request->all());
        return $this->sendResponse($id, "CREADO CORRECTAMENTE");
    }
    //Listar doctores
    public function getDoctor()
    {

        $result = DB::table('doctores')
            ->select('doctores.id', 'doctores.nombre', 'doctores.apellidos', 'doctores.fecha_nacimiento', 
            'especialidades.nombre as especialidad', 'doctores.direccion', 'especialidades.id as especialidad_id')
            ->join('especialidades', 'especialidades.id', '=', 'doctores.especialidad_id')
            ->get();
        return $this->sendResponse($result, "LISTA DOCTORES RECUPERADA");
    }
}
