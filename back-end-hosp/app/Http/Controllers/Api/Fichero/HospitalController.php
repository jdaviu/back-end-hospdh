<?php

namespace App\Http\Controllers\Api\Fichero;

use App\Models\Api\Ficheros\Hospital;
use App\Http\Controllers\Api\Fichero\BaseController;
use App\Http\Requests\Api\Ficheros\Hospital\StoreRequest;
use App\Http\Requests\Api\Ficheros\Hospital\UpdateRequest;
use Illuminate\Http\Request;

class HospitalController extends BaseController
{
    //
      //Listar
      public function index()
      {
          $list = Hospital::all();
          return $this->sendResponse($list, "LISTA RECUPERADA");
      }
      //Recuperar por id
      public function show(Hospital $id)
      {
          return $this->sendResponse($id, "ID RECUPERADO");
      }
      //Editar  por id
      public function update(UpdateRequest $request, Hospital $id)
      {
          $id->update($request->all());
          return $this->sendResponse($id, "ID EDITADO");
      }
      //Eliminar  por id
      public function destroy(Hospital $id)
      {
          $id->delete();
          return $this->sendResponse($id, "ID ELIMINADO");
      }
  
      //Crear
      public function store(StoreRequest $request)
      {
          $id = Hospital::create($request->all());
          return $this->sendResponse($id, "CREADO CORRECTAMENTE");
      }
}
