<?php

namespace App\Http\Controllers\Api\Fichero;

use App\Models\Api\Ficheros\Especialidad;
use App\Http\Controllers\Api\Fichero\BaseController;
use App\Http\Requests\Api\Ficheros\Especialidad\StoreRequest;
use App\Http\Requests\Api\Ficheros\Especialidad\UpdateRequest;

use Illuminate\Http\Request;

class EspecialidadController extends BaseController
{
    //
      //Listar
      public function index()
      {
          $list = Especialidad::all();
          return $this->sendResponse($list, "LISTA RECUPERADA");
      }
      //Recuperar por id
      public function show(Especialidad $id)
      {
          return $this->sendResponse($id, "ID RECUPERADO");
      }
      //Editar  por id
      public function update(UpdateRequest $request, Especialidad $id)
      {
          $id->update($request->all());
          return $this->sendResponse($id, "ID EDITADO");
      }
      //Eliminar  por id
      public function destroy(Especialidad $id)
      {
          $id->delete();
          return $this->sendResponse($id, "ID ELIMINADO");
      }
  
      //Crear
      public function store(StoreRequest $request)
      {
          $id = Especialidad::create($request->all());
          return $this->sendResponse($id, "CREADO CORRECTAMENTE");
      }
}
