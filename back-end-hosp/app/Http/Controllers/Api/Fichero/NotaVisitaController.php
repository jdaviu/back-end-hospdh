<?php

namespace App\Http\Controllers\Api\Fichero;

use App\Models\Api\Ficheros\NotaVisita;
use App\Http\Controllers\Api\Fichero\BaseController;
use App\Http\Requests\Api\Ficheros\NotaVisita\StoreRequest;
use App\Http\Requests\Api\Ficheros\NotaVisita\UpdateRequest;

use Illuminate\Http\Request;

class NotaVisitaController extends BaseController
{
    //
    //Listar
    public function index()
    {
        $list = NotaVisita::all();
        return $this->sendResponse($list, "LISTA RECUPERADA");
    }
    //Recuperar por id
    public function show(NotaVisita $id)
    {
        return $this->sendResponse($id, "ID RECUPERADO");
    }
    //Editar  por id
    public function update(UpdateRequest $request, NotaVisita $id)
    {
        $id->update($request->all());
        return $this->sendResponse($id, "ID EDITADO");
    }
    //Eliminar  por id
    public function destroy(NotaVisita $id)
    {
        $id->delete();
        return $this->sendResponse($id, "ID ELIMINADO");
    }

    //Crear
    public function store(StoreRequest $request)
    {
        $id = NotaVisita::create($request->all());
        return $this->sendResponse($id, "CREADO CORRECTAMENTE");
    }
}
