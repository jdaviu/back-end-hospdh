<?php

namespace App\Http\Controllers\Api\Fichero;

use App\Models\Api\Ficheros\DoctorPacienteHospital;
use App\Http\Controllers\Api\Fichero\BaseController;
use App\Http\Requests\Api\Ficheros\DoctorPacienteHospital\StoreRequest;
use App\Http\Requests\Api\Ficheros\DoctorPacienteHospital\UpdateRequest;
use Illuminate\Http\Request;

class DoctorPacienteHospitalController extends BaseController
{
    //
    //Listar
    public function index()
    {
        $list = DoctorPacienteHospital::all();
        return $this->sendResponse($list, "LISTA RECUPERADA");
    }
    //Recuperar por id
    public function show(DoctorPacienteHospital $id)
    {
        return $this->sendResponse($id, "ID RECUPERADO");
    }
    //Editar  por id
    public function update(UpdateRequest $request, DoctorPacienteHospital $id)
    {
        $id->update($request->all());
        return $this->sendResponse($id, "ID EDITADO");
    }
    //Eliminar  por id
    public function destroy(DoctorPacienteHospital $id)
    {
        $id->delete();
        return $this->sendResponse($id, "ID ELIMINADO");
    }

    //Crear
    public function store(StoreRequest $request)
    {
        $id = DoctorPacienteHospital::create($request->all());
        return $this->sendResponse($id, "CREADO CORRECTAMENTE");
    }
}
